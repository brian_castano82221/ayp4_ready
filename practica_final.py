import re
import hashlib
import random
import string
from datetime import datetime
import tkinter as tk
from tkinter import messagebox, simpledialog

# Estructura de datos
matriz = []
password_tree = {}

# Funciones de validación
def validate_document(document):
    return document.isdigit()

def validate_password(password):
    pattern = re.compile(r'^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,}$')
    return pattern.match(password)

def generate_token():
    return ''.join(random.choices(string.ascii_letters + string.digits, k=16))

def validate_date(date):
    try:
        datetime.strptime(date, "%d/%m/%Y")
        return True
    except ValueError:
        return False

def validate_email(email):
    pattern = re.compile(r'^[\w\.-]+@[\w\.-]+\.\w+$')
    return pattern.match(email)

def validate_poli_email(email):
    pattern = re.compile(r'^[\w\.-]+@elpoli\.edu\.co$')
    return pattern.match(email)

# Funciones del programa
def register_user():
    document_type = simpledialog.askstring("Registro de Usuario", "Tipo de documento:")
    document = simpledialog.askstring("Registro de Usuario", "Documento:")
    if document_type.lower() == "cedula" and not validate_document(document):
        messagebox.showerror("Error", "Documento inválido. Debe ser numérico.")
        return
    
    name = simpledialog.askstring("Registro de Usuario", "Nombre:")
    
    password = simpledialog.askstring("Registro de Usuario", "Contraseña:", show='*')
    if not validate_password(password):
        messagebox.showerror("Error", "Contraseña débil. Debe tener al menos 8 caracteres, una letra mayúscula, una letra minúscula y un número.")
        return
    
    token = generate_token()
    
    birthdate = simpledialog.askstring("Registro de Usuario", "Fecha de nacimiento (dd/mm/aaaa):")
    if not validate_date(birthdate):
        messagebox.showerror("Error", "Fecha de nacimiento inválida.")
        return
    
    city = simpledialog.askstring("Registro de Usuario", "Ciudad:")
    
    email = simpledialog.askstring("Registro de Usuario", "Correo:")
    if not validate_email(email):
        messagebox.showerror("Error", "Correo inválido.")
        return
    if validate_poli_email(email):
        university = "Estudiante del Poli"
        messagebox.showinfo("Información", "Correo del Poli detectado. Eres estudiante del Poli.")
    else:
        university = simpledialog.askstring("Registro de Usuario", "Universidad:")
    
    user_data = [document_type, document, name, password, token, birthdate, city, email, university]
    matriz.append(user_data)
    password_tree[document] = len(matriz) - 1
    messagebox.showinfo("Éxito", "Usuario registrado con éxito.")

def recover_password():
    document = simpledialog.askstring("Recuperar Contraseña", "Documento:")
    email = simpledialog.askstring("Recuperar Contraseña", "Correo:")
    
    user_index = password_tree.get(document)
    if user_index is not None and matriz[user_index][7] == email:
        messagebox.showinfo("Recuperar Contraseña", f"Su contraseña es: {matriz[user_index][3]}")
    else:
        messagebox.showerror("Error", "Documento o correo incorrectos.")

def modify_password():
    document = simpledialog.askstring("Modificar Contraseña", "Documento:")
    
    user_index = password_tree.get(document)
    if user_index is not None:
        current_password = simpledialog.askstring("Modificar Contraseña", "Contraseña actual:", show='*')
        if matriz[user_index][3] == current_password:
            new_password = simpledialog.askstring("Modificar Contraseña", "Nueva contraseña:", show='*')
            if validate_password(new_password):
                matriz[user_index][3] = new_password
                messagebox.showinfo("Éxito", "Contraseña actualizada con éxito.")
            else:
                messagebox.showerror("Error", "Nueva contraseña débil. Debe tener al menos 8 caracteres, una letra mayúscula, una letra minúscula y un número.")
        else:
            messagebox.showerror("Error", "Contraseña actual incorrecta.")
    else:
        messagebox.showerror("Error", "Documento no encontrado.")

def simulate_login():
    document = simpledialog.askstring("Simular Ingreso", "Documento:")
    password = simpledialog.askstring("Simular Ingreso", "Contraseña:", show='*')
    
    user_index = password_tree.get(document)
    if user_index is not None and matriz[user_index][3] == password:
        messagebox.showinfo("Éxito", "Ingreso exitoso.")
    else:
        messagebox.showerror("Error", "Documento o contraseña incorrectos.")

def show_users():
    users_info = "\n".join([f"Documento: {user[1]}, Nombre: {user[2]}, Token: {user[4]}, Fecha de Nacimiento: {user[5]}, Ciudad: {user[6]}, Correo: {user[7]}, Universidad: {user[8]}" for user in matriz])
    messagebox.showinfo("Usuarios Inscritos", users_info)

def save_data():
    with open("C:/Users/Brian/Desktop/Repositorio_AYP4/ayp4_ready/user.txt", "w") as file:
        for user in matriz:
            file.write(",".join(user) + "\n")
    
    with open("C:/Users/Brian/Desktop/Repositorio_AYP4/ayp4_ready/passwords_tree.txt", "w") as file:
        for key, value in password_tree.items():
            file.write(f"{key},{value}\n")

def load_data():
    try:
        with open("C:/Users/Brian/Desktop/Repositorio_AYP4/ayp4_ready/user.txt", "r") as file:
            for line in file:
                matriz.append(line.strip().split(","))
        
        with open("C:/Users/Brian/Desktop/Repositorio_AYP4/ayp4_ready/passwords_tree.txt", "r") as file:
            for line in file:
                key, value = line.strip().split(",")
                password_tree[key] = int(value)
    except Exception as e:
        messagebox.showerror("Error", f"Error al cargar datos: {e}")

def main_menu():
    load_data()
    root = tk.Tk()
    root.title("Gestión de Claves de Usuario")

    frame = tk.Frame(root)
    frame.pack(pady=20, padx=20)

    tk.Button(frame, text="Registrar Usuario", command=register_user).grid(row=0, column=0, pady=10)
    tk.Button(frame, text="Recuperar Contraseña", command=recover_password).grid(row=1, column=0, pady=10)
    tk.Button(frame, text="Modificar Contraseña", command=modify_password).grid(row=2, column=0, pady=10)
    tk.Button(frame, text="Simular Ingreso", command=simulate_login).grid(row=3, column=0, pady=10)
    tk.Button(frame, text="Mostrar Todos los Usuarios", command=show_users).grid(row=4, column=0, pady=10)
    tk.Button(frame, text="Salir", command=lambda: [save_data(), root.destroy()]).grid(row=5, column=0, pady=10)

    root.mainloop()

if __name__ == "__main__":
    main_menu()

import time
import random
import numpy as np

def read_matrix_from_file(file_path):
    """Reads a matrix from a file and returns it as a list of lists"""
    with open(file_path, 'r') as file:
        matrix = [list(map(int, line.split())) for line in file]
    return matrix

def find_number_in_matrix(matrix, number):
    """Finds the number in the matrix and returns the position if found, else returns None"""
    start_time = time.perf_counter_ns()
    for i, row in enumerate(matrix):
        for j, element in enumerate(row):
            if element == number:
                end_time = time.time()
                return (i, j), end_time - start_time
    end_time = time.perf_counter_ns()
    return None, end_time - start_time

def perform_test(matrix, test_number, description):
    print(f"Test: {description}")
    position, duration = find_number_in_matrix(matrix, test_number)
    if position:
        print(f"Number found at position {position} in {duration:.6f} seconds.")
    else:
        print(f"Number not found in {duration:.6f} seconds.")
    print(f"Order of magnitude: O(n*m) where n and m are dimensions of the matrix")

# Read matrix from file
file_path = 'C:/Users/Brian/Desktop/Repositorio_AYP4/ayp4_ready/matrix.txt'  # Replace with the actual file path if different
matrix = read_matrix_from_file(file_path)

# Get matrix dimensions
rows = len(matrix)
cols = len(matrix[0])

# Define test numbers
test_numbers = [
    matrix[0][0],  # First position
    matrix[rows // 2][cols // 2],  # Middle position
    matrix[rows - 1][cols - 1],  # Last position
    random.randint(1000000, 2000000)  # Random number with 60% chance of not being in the matrix
]

# Perform tests
perform_test(matrix, test_numbers[0], "Number in the first position")
perform_test(matrix, test_numbers[1], "Number in a middle position")
perform_test(matrix, test_numbers[2], "Number in the last position")
perform_test(matrix, test_numbers[3], "Number probably not in the matrix")

import time
import random
import sys

# Función de búsqueda binaria con contador de frecuencias
def busqueda_binaria(lista, objetivo):
    inicio = 0
    fin = len(lista) - 1
    contador = 0  # Inicializar el contador de frecuencias
    while inicio <= fin:
        contador += 1  # Incrementar el contador en cada comparación
        medio = (inicio + fin) // 2
        if lista[medio] == objetivo:
            return medio, contador
        elif lista[medio] < objetivo:
            inicio = medio + 1
        else:
            fin = medio - 1
    return -1, contador  # Devolver el resultado y el contador de frecuencias

def calcular_memoria(lista):
    memoria_total = (
        sys.getsizeof(lista) + 
        len(lista) * sys.getsizeof(lista[0])
    )
    return memoria_total

# Generamos una lista de 1000 números aleatorios sin repetición en el rango del 1 al 1000
lista = random.sample(range(1, 2000), 1000)
lista.sort()
# Imprimimos la lista de números aleatorios
print("Lista de números aleatorios:")
print(lista)



# Pedimos al usuario que ingrese el número a buscar
objetivo = int(input("Ingrese el número que desea buscar en la lista: "))

# Realizamos la búsqueda binaria
inicio_busqueda = time.perf_counter_ns()
resultado, contador = busqueda_binaria(lista, objetivo)
final_busqueda = time.perf_counter_ns()

# Mostramos el resultado de la búsqueda
if resultado != -1:
    print(f"El elemento {objetivo} se encuentra en el índice {resultado}.")
else:
    print(f"El elemento {objetivo} no se encuentra en la lista.")

# Calculamos el tiempo de búsqueda y la memoria utilizada
tiempo_transcurrido = final_busqueda - inicio_busqueda
memoria_utilizada = calcular_memoria(lista)

# Mostramos el tiempo de búsqueda y el contador de frecuencias
print(f"Tiempo de búsqueda: {tiempo_transcurrido} nanosegundos")
print(f"Contador de frecuencia: {contador * 8 * len(lista)}")
print("Orden de magnitud: O(logn)")
print(f"Memoria utilizada: {memoria_utilizada} bytes")


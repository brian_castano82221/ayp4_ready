import time
import random
import sys

# Definir la clase Nodo
class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None

# Definir la clase ArbolDeBusquedaBinaria
class ArbolDeBusquedaBinaria:
    def __init__(self):
        self.raiz = None

    def insertar(self, valor):
        if self.raiz is None:
            self.raiz = Nodo(valor)
        else:
            self._insertar_recursivo(self.raiz, valor)

    def _insertar_recursivo(self, nodo, valor):
        if valor < nodo.valor:
            if nodo.izquierda is None:
                nodo.izquierda = Nodo(valor)
            else:
                self._insertar_recursivo(nodo.izquierda, valor)
        else:
            if nodo.derecha is None:
                nodo.derecha = Nodo(valor)
            else:
                self._insertar_recursivo(nodo.derecha, valor)

    def buscar(self, valor):
        global contador
        contador = 0
        return self._buscar_recursivo(self.raiz, valor)

    def _buscar_recursivo(self, nodo, valor):
        global contador
        contador += 1
        if nodo is None or nodo.valor == valor:
            return nodo

        if valor < nodo.valor:
            return self._buscar_recursivo(nodo.izquierda, valor)
        else:
            return self._buscar_recursivo(nodo.derecha, valor)

# Función para calcular la memoria utilizada por el árbol
def calcular_memoria(arbol):
    memoria_total = (
        sys.getsizeof(arbol) + sys.getsizeof(arbol.raiz) +
        contar_nodos(arbol.raiz) * sys.getsizeof(Nodo) +
        sys.getsizeof(contador)
    )
    return memoria_total

# Función auxiliar para contar nodos del árbol
def contar_nodos(nodo):
    if nodo is None:
        return 0
    return 1 + contar_nodos(nodo.izquierda) + contar_nodos(nodo.derecha)

# Ejemplo de uso
global contador
contador = 0

# Insertar números aleatorios en el árbol
arbol = ArbolDeBusquedaBinaria()
numeros = random.sample(range(2000), 1000)  # Números aleatorios sin repetición
for numero in numeros:
    arbol.insertar(numero)

print(f"Números insertados en el árbol: {numeros}")

# Buscar un número aleatorio en el árbol
objetivo = random.choice(numeros)
inicio_busqueda = time.perf_counter_ns()
nodo = arbol.buscar(objetivo)
final_busqueda = time.perf_counter_ns()

if nodo:
    print(f"Elemento {nodo.valor} encontrado en el árbol.")
else:
    print("Elemento no encontrado en el árbol.")

# Calcular tiempo de búsqueda
tiempo_transcurrido = final_busqueda - inicio_busqueda

# Calcular memoria utilizada
memoria_utilizada = calcular_memoria(arbol)

print(f"Tiempo de búsqueda: {tiempo_transcurrido} nanosegundos")
print("Orden de magnitud: O(logn)")
print(f"Memoria utilizada: {memoria_utilizada} bytes")
print(f"Contador de frecuencia: {contador * 8 * len(numeros)}")

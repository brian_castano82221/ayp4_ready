import random
import time

cantidad = int(input("Ingrese la cantidad de datos que sea menor a 500:\n"))

# Verificar que la cantidad sea mayor a 1000
while cantidad >= 500:
    cantidad = int(input("Por favor, ingrese una cantidad mayor a 500:\n"))

class TablaHash:
    def __init__(self, capacidad):
        self.capacidad = capacidad
        self.tabla = [[] for _ in range(capacidad)]

    def funcion_hash(self, clave):
        # Simplemente tomar el módulo de la clave con la capacidad de la tabla
        return clave % self.capacidad

    def agregar(self, clave, valor):
        indice = self.funcion_hash(clave)
        # Si la posición está ocupada, realizar sondaje lineal
        while self.tabla[indice]:
            # Avanzar al siguiente índice (con envoltura al llegar al final de la tabla)
            indice = (indice + 1) % self.capacidad
        # Agregar el par clave-valor a la tabla en el índice calculado
        self.tabla[indice].append((clave, valor))

    def obtener(self, clave):
        indice = self.funcion_hash(clave)
        indice_inicial = indice
    # Iterar sobre los pares clave-valor en la tabla hash
        while self.tabla[indice]:
        # Si se encuentra la clave, devolver el valor asociado y el índice actual
            if self.tabla[indice][0][0] == clave:
                return self.tabla[indice][0][1], indice
        # Avanzar al siguiente índice (con envoltura al llegar al final de la tabla)
            indice = (indice + 1) % self.capacidad
        # Si hemos vuelto al índice inicial, salir del bucle para evitar un bucle infinito
            if indice == indice_inicial:
                break
    # Si no se encuentra la clave, devolver None y el índice inicial
        return None, indice_inicial


# Medir el tiempo de generación de la lista de números aleatorios y la inserción en la tabla hash
inicio_generacion = time.perf_counter_ns()

# Inicializar un conjunto para mantener números únicos
numeros_generados = set()
tabla = TablaHash(500)

while len(numeros_generados) < cantidad:
    numero_aleatorio = random.randint(1, 5000)
    if numero_aleatorio not in numeros_generados:
        # Usamos el número aleatorio como el valor asociado a la clave
        tabla.agregar(numero_aleatorio, numero_aleatorio)
        numeros_generados.add(numero_aleatorio)

fin_generacion = time.perf_counter_ns()

print("Tabla Hash:")
for i in range(tabla.capacidad):
    if tabla.tabla[i]:  # Solo imprimir cubos no vacíos
        print(f"Cubo {i}: {tabla.tabla[i][0][0]}, Posición actual en la tabla: {i}")
    else:
        print(f"Cubo {i}: Vacío")  # Imprimir cubos vacíos

print(f"Tiempo de generación e inserción de datos: {fin_generacion - inicio_generacion} nanosegundos")

clave_buscar = ""
while clave_buscar != "salir":
    clave_buscar = input("Ingrese la clave a buscar (o 'salir' para terminar): \n")
    if clave_buscar == "salir":
        print("Adios")
        break
    inicio_busqueda = time.perf_counter_ns()
    valor, indice_actual = tabla.obtener(int(clave_buscar))
    fin_busqueda = time.perf_counter_ns()
    print(f"El valor asociado a la clave {clave_buscar} es: {valor}")
    print(f"Posición actual en la tabla: {indice_actual}")
    indice_anterior = int(clave_buscar) % 500
    if indice_actual != indice_anterior:
        print(f"Posición anterior en la tabla: {indice_anterior}")
    print(f"Tiempo de búsqueda: {fin_busqueda - inicio_busqueda} nanosegundos")


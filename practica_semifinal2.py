import random
import time

def bubble_sort_for(A):
    n = len(A)
    for k in range(n-1):
        for l in range(k+1, n):
            if A[l] < A[k]:
                A[l], A[k] = A[k], A[l]  # Swap elements

def bubble_sort_while(A):
    n = len(A)
    k = 0
    while k < n-1:
        l = k + 1
        while l < n:
            if A[l] < A[k]:
                A[l], A[k] = A[k], A[l]  # Swap elements
            l += 1
        k += 1

n = int(input("Ingrese n: "))
A = []

for i in range(n):
    numero_aleatorio = random.randint(1, 2000)
    if numero_aleatorio not in A:
        A.append(numero_aleatorio)

# Copy the original list for sorting with both methods
A_for = A.copy()
A_while = A.copy()

# Record the start time for sorting with for loop
start_time_for = time.time()
bubble_sort_for(A_for)
end_time_for = time.time()

# Record the start time for sorting with while loop
start_time_while = time.time()
bubble_sort_while(A_while)
end_time_while = time.time()

# Print the sorted list for both methods
print("Lista ordenada con for loop:")
print(A_for)
print("Lista ordenada con while loop:")
print(A_while)

# Calculate and print the sorting time for both methods
sorting_time_for = end_time_for - start_time_for
sorting_time_while = end_time_while - start_time_while
print("Tiempo de ordenamiento con for loop:", sorting_time_for, "segundos")
print("Tiempo de ordenamiento con while loop:", sorting_time_while, "segundos")
